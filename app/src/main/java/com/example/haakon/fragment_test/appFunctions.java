package com.example.haakon.fragment_test;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by warce_000 on 05.10.2016.
 */
public final class appFunctions {

    public static void popUp(String popU, Context context) {
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, popU, duration);
        toast.show();
    }
}
