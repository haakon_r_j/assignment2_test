package com.example.haakon.fragment_test;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.SyncStateContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class ImageAvtivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView image;
    private TextView text;
    private File file;
    private String path;
    private DatabaseHelper db;
    private Button deletePic;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_avtivity);

        db = new DatabaseHelper(this);
        deletePic = (Button) findViewById(R.id.deleteColumn);

        Bundle bd = getIntent().getExtras();
        name = bd.getString("name");
        ImageView image = (ImageView) findViewById(R.id.showImage);

        if(!name.isEmpty()) {
            File largePicture = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "pictures");
            path = largePicture.getAbsolutePath();
            image.setImageBitmap(loadImageFromStorage(path, name));
            image.setRotation(90);
        }
        else {
            Toast.makeText(this, "Cant find picture name", Toast.LENGTH_LONG).show();
        }

        deletePic.setOnClickListener(this);
    }

    private Bitmap loadImageFromStorage(String path, String name)
    {
        Bitmap b;
        try {
                File f = new File(path, name);
                b = BitmapFactory.decodeStream(new FileInputStream(f));
        }
        catch (FileNotFoundException e)
        {
            b = null;
            e.printStackTrace();
        }
        return b;
    }
    @Override
    public void onClick(View v) {
       if(v == deletePic){ //If delete button was pressed asks user continue/cancel
            new AlertDialog.Builder(this)
                    .setTitle("Delete image")
                    .setMessage("Are you sure you want to delete this image?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (deletePicture()) { //if delete cords returns true, it was not a success
                                appFunctions.popUp("Picture deleted", getApplicationContext()); // success popUp
                               // Intent intent = new Intent(ImageAvtivity.this, Gallery.class);
                               // startActivity(intent);
                            }
                            else appFunctions.popUp("Something went wrong", getApplicationContext()); // error popUp
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .show();
        }
    }

    public boolean deletePicture() {
        File largePicture = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "/pictures"), name);
        File smallPicture = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "/thumbnails"), name);

            if(db.deleteColumn(name)) {
                largePicture.delete();
                smallPicture.delete();
                return true;

            }
             else return false;
    }
}
