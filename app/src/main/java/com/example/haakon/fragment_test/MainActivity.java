package com.example.haakon.fragment_test;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

//Find another solution for return b on bitmap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private Button gallery;
    private Button takePicture;
    private Button dbDelete;
    private TextView text;
    private ImageView image;
    private String name;
    private Button takePictureButton;
    private ImageView imageView;
    private Uri file;

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gallery = (Button) findViewById(R.id.button);
        takePicture = (Button) findViewById(R.id.button3);
        dbDelete = (Button) findViewById(R.id.dbDelete);

        //text = (TextView) findViewById(R.id.showText);

        imageView = (ImageView) findViewById(R.id.imageView);

        db = new DatabaseHelper(this);

        String name1 = "";
        name1 = db.getName(0, name1);

        File largePicture = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "pictures");
        String path = largePicture.getAbsolutePath();
       // name = mediaStorageDir.getName();
        Bitmap b;

        try {
           File f = new File(path, name1);
            b = BitmapFactory.decodeStream(new FileInputStream(f));
        }
        catch (FileNotFoundException e)
        {
            b = null;
            e.printStackTrace();
        }
        imageView.setImageBitmap(b);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            takePictureButton.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        gallery.setOnClickListener(this);
        takePicture.setOnClickListener(this);
        dbDelete.setOnClickListener(this);
    }

        @Override
        public void onClick(View v) {
            if(v == gallery) {
                Intent intent = new Intent(MainActivity.this, Gallery.class);
                startActivity(intent);
            }
            else if(v == takePicture) {
                takePicture();
            }
            else if(v == dbDelete){ //If delete button was pressed asks user continue/cancel
                new AlertDialog.Builder(this)
                        .setTitle("Delete data")
                        .setMessage("Are you sure you want to delete all data?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (db.emtyTheDatabase()) { //if delete cords returns true, it was not a success
                                    popUpp("error"); // error popUpp
                                }
                                //Pop upp for success!!
                                else popUpp("success");
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //do nothing
                            }
                        })
                        .show();
            }
        }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takePictureButton.setEnabled(true);
            }
        }
    }

    public void takePicture() {
        name = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile(name));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, 100);
    }

    private static File getOutputMediaFile(String timeStamp) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "pictures");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("pictures", "failed to create directory");
                return null;
            }
        }
        return new File(mediaStorageDir.getPath() + File.separator +
                timeStamp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                db.addEntry(name);
                //text.setText(name);
                //imageView.setImageURI(file);
                File largePicture = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES + "/pictures"), name);
                Bitmap myBitmap = BitmapFactory.decodeFile(largePicture.getAbsolutePath());
                Bitmap thumbnail = Bitmap.createScaledBitmap(myBitmap, 200, 200, false);
                if (saveImageToExternalStorage(thumbnail, name)) {
                    Toast.makeText(this, "Picture stored", Toast.LENGTH_LONG).show();
                } else Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    public final static String PATH = "/Pictures/";
    public final static String APP_THUMBNAIL_PATH_SD_CARD = "thumbnails";

    public boolean saveImageToExternalStorage(Bitmap image, String name) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + PATH + APP_THUMBNAIL_PATH_SD_CARD;

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            OutputStream fOut = null;
            File file = new File(fullPath, name);
            file.createNewFile();
            fOut = new FileOutputStream(file);

            // 100 means no compression, the lower you go, the stronger the compression
            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            //this would store the picture again under the main Picture folder and is not needed
            // MediaStore.Images.Media.insertImage(this.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            return true;

        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }
    public void popUpp(String popU) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, popU, duration);
        toast.show();
    }
}


