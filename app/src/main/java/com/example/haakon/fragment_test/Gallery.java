package com.example.haakon.fragment_test;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Gallery extends AppCompatActivity implements View.OnClickListener{
    private int count;
    private Bitmap[] thumbnails;
    private boolean[] thumbnailsselection;
    private String[] arrPath;
    private ImageAdapter imageAdapter;
    private GridView imageGrid;
    private DatabaseHelper db;
    private Button deleteAll;
    ArrayList<String> f = new ArrayList<String>();// list of file paths
    File file;
    File[] listFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        db = new DatabaseHelper(this);

        deleteAll = (Button) findViewById(R.id.deleteAll);

        deleteAll.setOnClickListener(View.this);

        getFromSdcard();
        imageGrid = (GridView) findViewById(R.id.gridView);
        imageAdapter = new ImageAdapter();
        imageGrid.setAdapter(imageAdapter);

        imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String name = imageAdapter.getName(position);
                Intent intent = new Intent(Gallery.this, ImageAvtivity.class);
                intent.putExtra("name", name);
                intent.putExtra("index", position);
                startActivity(intent);
            }
        });

        imageGrid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                //imageGrid = (GridView) findViewById(R.id.gridView);
                //imageAdapter = new ImageAdapter();
                //imageGrid.setAdapter(imageAdapter);
                //imageAdapter.checkBox(pos, arg1);
                popUp("Does nothing yet");
                return true;
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        //imageAdapter.notifyDataSetChanged();
        //imageGrid.setAdapter(imageAdapter);
    }

    public void getFromSdcard() {
        File file = new File(android.os.Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "thumbnails");
        listFile = file.listFiles();

        for (int i = 0; i < listFile.length; i++) {
            f.add(listFile[i].getAbsolutePath());
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public ImageAdapter() {
            mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        //function that show checkboxes for each picture
       /* public View checkBox (int pos, View convertView) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(
                        R.layout.grid_item_layout, null);
                holder.checkbox = (CheckBox) findViewById(R.id.checkBox);

                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.checkbox.setVisibility(View.VISIBLE);
            return convertView;
        }*/

        public String getName(int position) {
            File file = new File((f.get(position)));
            String name = file.getName();
            return name;
        }

        public int getCount() {
            return f.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(
                        R.layout.grid_item_layout, null);
                holder.imageview = (ImageView) convertView.findViewById(R.id.image);
                holder.textView = (TextView) convertView.findViewById(R.id.textView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            File file = new File((f.get(position)));
            String name = file.getName();
            Bitmap myBitmap = BitmapFactory.decodeFile(f.get(position));
            holder.textView.setText(name);
            holder.imageview.setImageBitmap(myBitmap);
            return convertView;
        }
    }

        class ViewHolder {
            ImageView imageview;
            TextView textView;
            CheckBox checkBox;
        }

        public void popUp(String popU) {
            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, popU, duration);
            toast.show();
        }

}


