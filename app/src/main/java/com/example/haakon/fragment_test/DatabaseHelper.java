package com.example.haakon.fragment_test;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final String DATABASE_NAME = "pictures";
    private static final String DB_TABLE = "picturesData";
    private static final String COLUMN_ID = "id"; //equal to position in grid
    private static final String KEY_NAME = "imageName";

    private static final int DATABASE_VERSION = 3;



    public DatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating table
        String sql = "CREATE TABLE " +DB_TABLE
                +"(" +COLUMN_ID+
                " INTEGER PRIMARY KEY AUTOINCREMENT, " +KEY_NAME+ " VARCHAR);";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //on upgrade drop older tables
        String sql = "DROP TABLE IF EXISTS picturesData";
        db.execSQL(sql);

        // create new table
        onCreate(db);
    }

    public boolean addEntry(String name) throws SQLiteException {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_NAME, name);

        db.insert(DB_TABLE, null, contentValues);
        db.close();
        return true;
    }

    public boolean emtyTheDatabase(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "DELETE FROM " +DB_TABLE+ ";";
        db.execSQL(sql);

        sql = "UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='picturesData';";
        db.execSQL(sql);
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        return c.moveToFirst();
    }

    public Cursor fetchName(int position) {
        //position starts at 0 and id 1, ++ to make them match picture in database
        position++;
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT " +KEY_NAME+ " FROM " +DB_TABLE+ " WHERE " +COLUMN_ID+ " = "+position+";";

        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public String getName(int position, String name){
        Cursor c = fetchName(position);
        c.moveToFirst();
        if (c.moveToFirst()) {
            name = c.getString(c.getColumnIndex(DatabaseHelper.KEY_NAME));
        }
        c.close();
        return name;
    }

    //deletes a column and updates id
    public boolean deleteColumn (String name) {
        SQLiteDatabase db = this.getReadableDatabase();

        int id;

        String sql = "SELECT id, * FROM " + DB_TABLE + " WHERE " + KEY_NAME + " = '" + name + "'";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        if (c.moveToFirst()) {
            c.moveToFirst();
            id = c.getInt(c.getColumnIndex(DatabaseHelper.COLUMN_ID));
            sql = "DELETE FROM " + DB_TABLE + " WHERE " + KEY_NAME + " = '" + name + "'";
            db.execSQL(sql);

            //Check if the database is empty
            sql = "SELECT * FROM " +DB_TABLE;
            c = db.rawQuery(sql, null);
            c.moveToFirst();
            //if db is not empty
            if (c.moveToFirst()) {
                //subtract 1 from id where id's larger then the deleted's column
                sql = "UPDATE " + DB_TABLE + " SET id = id - 1 WHERE id > " + id + ";";
                db.execSQL(sql);

                c.close();
                return true;
            //db is empty. reset AUTOINCREMENT ID to 0
            } else {
                sql = "UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='" + DB_TABLE + "';";

                db.execSQL(sql);
                c.close();
                return true;

            }
        }
        else {
                return false;
            }
    }




    public int getMaxId() {
        SQLiteDatabase db = this.getReadableDatabase();

        int id;

        String sql = "SELECT " +COLUMN_ID+ " FROM " + DB_TABLE + " WHERE id = (SELECT MAX(id) FROM " +DB_TABLE+ ");";
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            id = c.getInt(c.getColumnIndex(DatabaseHelper.COLUMN_ID));
        }
        else {
            c.close();
            return id = 0;
        }
        c.close();
        return id;
    }
}
